package fr.progcomp.hepaylike;

import fr.progcomp.hepaylike.Categories.Category;
import fr.progcomp.hepaylike.Categories.CategoryService;
import fr.progcomp.hepaylike.Products.Product;
import fr.progcomp.hepaylike.Products.ProductService;
import fr.progcomp.hepaylike.Users.User;
import fr.progcomp.hepaylike.Users.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Collection;

@SpringBootApplication
public class HepaylikeApplication
{
	public static void main(String[] args) {
		SpringApplication.run(HepaylikeApplication.class, args);
	}

	@Bean
	public CommandLineRunner run(CategoryService categoryService, UserService userService, ProductService productService)
	{
		// Creation des categories
		return (args) -> {
			System.out.println("Création des données de test...");

			if(categoryService.getCategory(1) == null) {
				categoryService.createCategory(new Category(1, "Animalerie"));
			}
			if(categoryService.getCategory(2) == null) {
				categoryService.createCategory(new Category(2, "Véhicules"));
			}
			if(categoryService.getCategory(3) == null) {
				categoryService.createCategory(new Category(3, "Musique"));
			}
			if(categoryService.getCategory(4) == null) {
				categoryService.createCategory(new Category(4, "Film"));
			}
			if(categoryService.getCategory(5) == null) {
				categoryService.createCategory(new Category(5, "Livre"));
			}
			if(categoryService.getCategory(6) == null) {
				categoryService.createCategory(new Category(6, "Informatique"));
			}
			if(categoryService.getCategory(7) == null) {
				categoryService.createCategory(new Category(7, "Jeux et Jouets"));
			}
			if(categoryService.getCategory(8) == null) {
				categoryService.createCategory(new Category(8, "Accessoire"));
			}
			if(categoryService.getCategory(9) == null) {
				categoryService.createCategory(new Category(9, "Vêtement"));
			}
			if(categoryService.getCategory(10) == null) {
				categoryService.createCategory(new Category(10, "Puériculture"));
			}
			if(categoryService.getCategory(11) == null) {
				categoryService.createCategory(new Category(11, "Sport"));
			}
			if(categoryService.getCategory(12) == null) {
				categoryService.createCategory(new Category(12, "Beauté et Bien-Être"));
			}
			if(categoryService.getCategory(13) == null) {
				categoryService.createCategory(new Category(13, "Bricolage"));
			}
			if(categoryService.getCategory(14) == null) {
				categoryService.createCategory(new Category(14, "Cuisine"));
			}
			if(categoryService.getCategory(15) == null) {
				categoryService.createCategory(new Category(15, "Maison"));
			}

			System.out.println("Catégories créées...");

			if(userService.getUserByEmailAndPwd("acheteur@gmail.com", "azerty") == null) {
				User u  = new User("Pierre", "HUGUES", "My Address in France", "acheteur@gmail.com", "azerty");
				userService.createUser(u);
			}

			if(userService.getUserByEmailAndPwd("vendeur@gmail.com", "azerty") == null) {
				User u  = new User("Roxanne", "THOMAS", "My Address in France", "vendeur@gmail.com", "azerty");
				userService.createUser(u);
			}

			if(userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty") == null) {
				User u  = new User("Arnaud", "BERGINE", "My Address in France", "vendeur2@gmail.com", "azerty");
				userService.createUser(u);
			}

			System.out.println("Utilisateurs créés...");

			if(((Collection<Product>) productService.getProductsByName("Ordinateur")).size() == 0) {
				Product p = new Product("Ordinateur", "MSI", 150.20, categoryService.getCategory(6), userService.getUserByEmailAndPwd("vendeur@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Epée de Vérité")).size() == 0) {
				Product p = new Product("Epée de Vérité", "1er Tome", 20, categoryService.getCategory(5), userService.getUserByEmailAndPwd("vendeur@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Voiture")).size() == 0) {
				Product p = new Product("Voiture", "Toyota Yaris", 15000, categoryService.getCategory(2), userService.getUserByEmailAndPwd("vendeur@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Polo")).size() == 0) {
				Product p = new Product("Polo", "Volkswagen Polo", 1500, categoryService.getCategory(2), userService.getUserByEmailAndPwd("vendeur@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Chiot")).size() == 0) {
				Product p = new Product("Chiot", "Chiot de cirque de sept mois qui a tendance à piquer de la viande en cuisine.", 1000, categoryService.getCategory(1), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Ordinateur portable")).size() == 0) {
				Product p = new Product("Ordinateur portable", "Dell", 550, categoryService.getCategory(6), userService.getUserByEmailAndPwd("vendeur@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Ordinateur portable 2")).size() == 0) {
				Product p = new Product("Ordinateur portable 2", "Asus", 850, categoryService.getCategory(6), userService.getUserByEmailAndPwd("vendeur@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Ordinateur portable 3")).size() == 0) {
				Product p = new Product("Ordinateur portable 3", "Acer", 400, categoryService.getCategory(6), userService.getUserByEmailAndPwd("vendeur@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Ordinateur portable 4")).size() == 0) {
				Product p = new Product("Ordinateur portable 4", "Mac sans accessoire", 4500, categoryService.getCategory(6), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Scie sauteuse")).size() == 0) {
				Product p = new Product("Scie sauteuse", "Scie sauteuse électrique, fonctionnelle, quelques rayures.", 15, categoryService.getCategory(13), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Bétonière")).size() == 0) {
				Product p = new Product("Bétonière", "Prêt d'une bétonière pour la journée.", 50, categoryService.getCategory(13), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Lot de tournevis")).size() == 0) {
				Product p = new Product("Lot de tournevis", "Lot de 10 tournevis cruciformes toutes tailles.", 20, categoryService.getCategory(13), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Collier rouge")).size() == 0) {
				Product p = new Product("Collier rouge", "Collier pour chien rouge, taille M", 5, categoryService.getCategory(1), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Collier bleu")).size() == 0) {
				Product p = new Product("Collier bleu", "Collier pour chien bleu, taille M", 5, categoryService.getCategory(1), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Collier jaune")).size() == 0) {
				Product p = new Product("Collier jaune", "Collier pour chien jaune, taille M", 5, categoryService.getCategory(1), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Collier vert")).size() == 0) {
				Product p = new Product("Collier vert", "Collier pour chien bleu, taille M", 5, categoryService.getCategory(1), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Collier orange")).size() == 0) {
				Product p = new Product("Collier orange", "Collier pour chien jaune, taille M", 5, categoryService.getCategory(1), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Griffoir")).size() == 0) {
				Product p = new Product("Griffoir", "Griffoir pour chat", 20, categoryService.getCategory(1), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Arbre à chat")).size() == 0) {
				Product p = new Product("Arbre à chat", "Arbre à chat neuf, 68cm*68cm*152cm", 50, categoryService.getCategory(1), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), true);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Pantalon noir")).size() == 0) {
				Product p = new Product("Pantalon noir", "Pantalon noir, disponible en taille S, M, L, XL", 30, categoryService.getCategory(9), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Pantalon blanc")).size() == 0) {
				Product p = new Product("Pantalon blanc", "Pantalon blanc, disponible en taille S, M, L, XL", 30, categoryService.getCategory(9), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("Pantalon beige")).size() == 0) {
				Product p = new Product("Pantalon beige", "Pantalon beige, disponible en taille S, M, L, XL", 30, categoryService.getCategory(9), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("T-shirt noir")).size() == 0) {
				Product p = new Product("T-shirt noir", "T-shirt noir, disponible en taille S, M, L, XL", 20, categoryService.getCategory(9), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("T-shirt blanc")).size() == 0) {
				Product p = new Product("T-shirt blanc", "T-shirt blanc, disponible en taille S, M, L, XL", 20, categoryService.getCategory(9), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			if(((Collection<Product>) productService.getProductsByName("T-shirt beige")).size() == 0) {
				Product p = new Product("T-shirt beige", "T-shirt beige, disponible en taille S, M, L, XL", 20, categoryService.getCategory(9), userService.getUserByEmailAndPwd("vendeur2@gmail.com", "azerty"), false);
				productService.createProduct(p);
			}

			System.out.println("Produits créés...");

			System.out.println("Données de test créés...");
		};
	}
}

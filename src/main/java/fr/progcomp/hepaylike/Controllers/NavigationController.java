package fr.progcomp.hepaylike.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NavigationController
{
    @GetMapping("/index")
    public String index() {
        return "index";
    }

    // Navbar
    @GetMapping("/navbar")
    public String navbar() {
        return "navbar";
    }

    @GetMapping("/commandHistory")
    public String commandHistory() {
        return "commandHistory";
    }
}

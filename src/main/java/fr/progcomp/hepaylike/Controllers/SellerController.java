package fr.progcomp.hepaylike.Controllers;

import fr.progcomp.hepaylike.Products.Product;
import fr.progcomp.hepaylike.Products.ProductRepository;
import fr.progcomp.hepaylike.Products.ProductServiceImpl;
import fr.progcomp.hepaylike.Users.User;
import fr.progcomp.hepaylike.Users.UserRepository;
import fr.progcomp.hepaylike.Users.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Collection;

@Controller
public class SellerController {

    @Autowired
    UserServiceImpl usi;

    @Autowired
    ProductServiceImpl psi;

    @Resource
    ProductRepository pr;

    @GetMapping("/mySells")
    public String mySells() {
        return "mySells";
    }

    @PostMapping(path="/createProduct")
    public String createProduct(@RequestBody Product product) {

        psi.createProduct(product);

        return "redirect:/mySells";
    }

    @GetMapping(path="/getMyProducts/{id}")
    public @ResponseBody ResponseEntity<Iterable<Product>> getMyProducts(@PathVariable int id) {
        User seller = usi.getUser(id);

        return new ResponseEntity<>(pr.findBySeller(seller), HttpStatus.OK);
    }

    @GetMapping(path="/getMyPourchases/{id}")
    public @ResponseBody ResponseEntity<Collection<Product>> getMyPourchases(@PathVariable int id) {
        User seller = usi.getUser(id);

        Collection<Product> products = seller.getUserItems();

        for(Product product : products) {
            if(product.getBuyer() != null) {
                product.setBuyer(new User(product.getBuyer().getFirstName(), product.getBuyer().getLastName()));
            }
            if(product.getBidder() != null) {
                product.setBidder(new User(product.getBidder().getFirstName(), product.getBidder().getLastName()));
            }
            product.setSeller(new User(product.getSeller().getFirstName(), product.getSeller().getLastName()));
        }

        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}

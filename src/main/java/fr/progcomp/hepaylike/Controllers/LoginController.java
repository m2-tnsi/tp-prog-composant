package fr.progcomp.hepaylike.Controllers;

import fr.progcomp.hepaylike.Users.User;
import fr.progcomp.hepaylike.Users.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

    @Autowired
    UserServiceImpl usi;

    @GetMapping("/connection")
    public String connection() {
        return "connection";
    }

    @PostMapping(path="/login")
    public @ResponseBody ResponseEntity<String> login(String email, String pwd) {

        User user = usi.getUserByEmailAndPwd(email, pwd);

        if(user == null) {
            return new ResponseEntity<String>("No user found", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("User", user.toJson());

        return new ResponseEntity<String>("Logged", headers, HttpStatus.OK);
    }

}

package fr.progcomp.hepaylike.Controllers;

import fr.progcomp.hepaylike.Users.User;


import fr.progcomp.hepaylike.Users.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class InscriptionController {

    @Autowired
    UserServiceImpl usi;

    @GetMapping("/inscription")
    public String inscription() {
        return "inscription";
    }

    @PostMapping(path="/inscription")
    public @ResponseBody ResponseEntity<String> addMember(@RequestBody User user) {

        User existingUser = usi.getUserByEmailAndPwd(user.getEmail(), user.getPwd());

        if(existingUser != null) {
            return new ResponseEntity<String>("This user already exists", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        usi.createUser(user);

        return new ResponseEntity<String>("Logged", HttpStatus.OK);
    }
}

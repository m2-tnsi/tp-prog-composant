package fr.progcomp.hepaylike.Controllers;

import fr.progcomp.hepaylike.Products.Product;
import fr.progcomp.hepaylike.Products.ProductRepository;
import fr.progcomp.hepaylike.Products.ProductServiceImpl;
import fr.progcomp.hepaylike.Users.User;
import fr.progcomp.hepaylike.Users.UserRepository;
import fr.progcomp.hepaylike.Users.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Optional;

@Controller
public class ProductsController {

    @Autowired
    ProductServiceImpl psi;

    @Autowired
    UserServiceImpl usi;

    @Resource
    ProductRepository pr;

    @Resource
    UserRepository ur;

    @GetMapping(path="/products")
    public String getAllProducts(Model model) {


        System.out.println("Produits : ");
        System.out.println(pr.findAll());
        model.addAttribute("products", pr.findAll());
        return "products";
    }

    @GetMapping(path="productDetails/{id}")
    public String getProductDetails(@PathVariable int id, Model model) throws Exception
    {
        Optional<Product> productOptional = pr.findById(id);
        if(productOptional.isPresent())
        {
            model.addAttribute("product", productOptional.get());
        }
        else
        {
            throw new Exception("Le produit n'a pas été trouvé.");
        }
        return "productDetails";
    }

    @PostMapping(path="/buy/{id}")
    public @ResponseBody ResponseEntity<String> buyProduct(@PathVariable int id, int userId)
    {
        User user = usi.getUser(userId);

        Product product = psi.getProduct(id);
        product.setSold(true);
        product.setBuyer(user);

        user.addUserItems(product);

        pr.save(product);
        ur.save(user);

        return new ResponseEntity<String>("ProductSell", HttpStatus.OK);
    }

    @PostMapping(path="/setProduct/{id}")
    public @ResponseBody ResponseEntity<String> setProduct(@PathVariable int id, int userId, double newPrice)
    {
        User user = usi.getUser(userId);
        Product product = psi.getProduct(id);

        product.setBidder(user);
        product.setCurrentPrice(newPrice);

        user.addUserItems(product);

        pr.save(product);
        ur.save(user);

        return new ResponseEntity<String>("ProductUpdate", HttpStatus.OK);
    }
}

package fr.progcomp.hepaylike.Users;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer>{

    User findByEmailAndPwd(String email, String password);
}

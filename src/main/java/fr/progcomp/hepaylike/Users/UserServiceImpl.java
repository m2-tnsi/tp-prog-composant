package fr.progcomp.hepaylike.Users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements UserService
{
    @Autowired
    public UserRepository userRepository;

    @Transactional
    public int createUser(User user)
    {
        return userRepository.save(user).getId();
    }

    @Transactional
    public void deleteUser(int idUser)
    {
        User u = userRepository.findById(idUser).orElse(null);
        if(u != null)
        {
            userRepository.delete(u);
        }
    }

    public User getUser(int id)
    {
        return userRepository.findById(id).orElse(null);
    }

    public User getUserByEmailAndPwd(String email, String pwd) {
        return userRepository.findByEmailAndPwd(email, pwd);
    }
}

package fr.progcomp.hepaylike.Users;

public interface UserService
{
    public int createUser(User user);
    public void deleteUser(int id);
    public User getUser(int id);
    public User getUserByEmailAndPwd(String email, String pwd);
}

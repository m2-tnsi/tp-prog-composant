package fr.progcomp.hepaylike.Users;

import fr.progcomp.hepaylike.Products.Product;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class User
{
    @Id
    @GeneratedValue
    @Column(name="USER_ID")
    private int userId;
    private String firstName;
    private String lastName;
    private String address;
    private String email;
    private String pwd;
    @ManyToMany
    private Collection<Product> userItems;

    public User(String firstName, String lastName, String address, String email, String pwd)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.email = email;
        this.pwd = pwd;
    }

    public User(String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User(){}

    public User(int id)
    {
        this.userId = id;
    }

    public int getId() {
        return userId;
    }

    public void setId(int id) {
        this.userId = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Collection<Product> getUserItems() {
        return userItems;
    }

    public void setUserItems(Collection<Product> userItems) {
        this.userItems = userItems;
    }

    public void addUserItems(Product product) {
        this.userItems.add(product);
    }

    public String toJson() {
        return String.format("{\"userId\": %d, \"firstName\": \"%s\", \"lastName\": \"%s\", \"address\": \"%s\", \"email\": \"%s\"}", userId, firstName, lastName, address, email);
    }
}

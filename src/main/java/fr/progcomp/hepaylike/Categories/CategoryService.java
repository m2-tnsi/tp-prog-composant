package fr.progcomp.hepaylike.Categories;

public interface CategoryService
{
    public int createCategory(Category cat);
    public void deleteCategory(int id);
    public Category getCategory(int id);
}

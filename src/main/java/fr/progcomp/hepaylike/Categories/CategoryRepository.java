package fr.progcomp.hepaylike.Categories;

import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Integer>
{
}

package fr.progcomp.hepaylike.Categories;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Category
{
    @Id
    @Column(name="CATEGORY_ID")
    private int categoryId;
    private String categoryName;

    public Category()
    {

    }

    public Category(int id, String categoryName) {
        this.categoryId = id;
        this.categoryName = categoryName;
    }

    public Category(String categoryName) {
        this.categoryName = categoryName;
    }

    public Category(int id)
    {
        this.categoryId = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}

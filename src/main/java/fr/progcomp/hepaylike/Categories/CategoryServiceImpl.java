package fr.progcomp.hepaylike.Categories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    public CategoryRepository categoryRepository;

    @Transactional
    public int createCategory(Category category)
    {
        return categoryRepository.save(category).getCategoryId();
    }

    @Transactional
    public void deleteCategory(int idCategory)
    {
        Category c = categoryRepository.findById(idCategory).orElse(null);
        if(c != null)
        {
            categoryRepository.delete(c);
        }
    }

    public Category getCategory(int id)
    {
        return categoryRepository.findById(id).orElse(null);
    }
}

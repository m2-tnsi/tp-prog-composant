package fr.progcomp.hepaylike.Products;

import fr.progcomp.hepaylike.Categories.Category;
import fr.progcomp.hepaylike.Users.User;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Product
{
    @Id
    @GeneratedValue
    private int productId;
    private String name;
    private String description;
    private double currentPrice;
    private double basePrice;
    @ManyToOne(optional=false)
    @JoinColumn(name="categoryId", referencedColumnName="CATEGORY_ID")
    private Category category;
    @ManyToOne(optional=false)
    @JoinColumn(name="sellerId", referencedColumnName="USER_ID")
    private User seller;
    private boolean isAuction;
    @ManyToOne
    @JoinColumn(name="bidderId", referencedColumnName="USER_ID", insertable=false)
    private User bidder;
    private boolean isSold;
    @ManyToOne
    @JoinColumn(name="buyerId", referencedColumnName="USER_ID", insertable=false)
    private User buyer;
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    public Product(int productId, String name, String description, double currentPrice, double basePrice, Category category, User seller, boolean isAuction, User bidder) {
        this.productId = productId;
        this.name = name;
        this.description = description;
        this.currentPrice = currentPrice;
        this.basePrice = basePrice;
        this.category = category;
        this.seller = seller;
        this.isAuction = isAuction;
        this.bidder = bidder;
    }

    public Product(String name, String description, double currentPrice, Category category, User seller, boolean isAuction) {
        this.name = name;
        this.description = description;
        this.currentPrice = currentPrice;
        this.basePrice = currentPrice;
        this.category = category;
        this.seller = seller;
        this.isAuction = isAuction;
        this.endDate = new Date(121, 1, 1);
    }
    public Product(String name){
        this.name = name;
    }

    public Product(){}

    public Product(int id)
    {
        this.productId = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public boolean getIsAuction() {
        return isAuction;
    }

    public void setAuction(boolean auction) {
        isAuction = auction;
    }

    public User getBidder() {
        return bidder;
    }

    public void setBidder(User bidder) {
        this.bidder = bidder;
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}

package fr.progcomp.hepaylike.Products;

import fr.progcomp.hepaylike.Users.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {

    @Query("Select productId, basePrice, currentPrice, category.categoryName, description, isAuction, isSold, endDate, name From Product Where seller=?1")
    Iterable<Product> findBySeller(User user);

    Iterable<Product> findByName(String name);
}

package fr.progcomp.hepaylike.Products;

import fr.progcomp.hepaylike.Users.User;

public interface ProductService
{
    public int createProduct(Product product);
    public void deleteProduct(int id);
    public Product getProduct(int id);
    public Iterable<Product> getProductsBySeller(User user);
    public Iterable<Product> getProductsByName(String name);
}

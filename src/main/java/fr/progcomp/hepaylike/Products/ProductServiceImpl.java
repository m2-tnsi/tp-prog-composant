package fr.progcomp.hepaylike.Products;

import fr.progcomp.hepaylike.Users.User;
import fr.progcomp.hepaylike.Users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ProductServiceImpl implements ProductService
{
    @Autowired
    public ProductRepository productRepository;

    @Transactional
    public int createProduct(Product product)
    {
        return productRepository.save(product).getProductId();
    }

    @Transactional
    public void deleteProduct(int idProduct)
    {
        Product p = productRepository.findById(idProduct).orElse(null);
        if(p != null)
        {
            productRepository.delete(p);
        }
    }

    @Override
    public Product getProduct(int id)
    {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Product> getProductsBySeller(User user) {
        return productRepository.findBySeller(user);
    }

    @Override
    public Iterable<Product> getProductsByName(String name) { return productRepository.findByName(name) ;}
}

# TP Prog Composant

## Equipe

Pierre HUGUES - Roxanne THOMAS - M2 TNSI FA

## Dépôt Gitlab

https://gitlab.com/m2-tnsi/tp-prog-composant/

## Fonctionnalités développées

- Un utilisateur peut s’inscrire
- Un utilisateur peut se connecter
- Les produits sont classés par catégories
- Le mode achat direct est disponible
- Le mode enchère est disponible
- Un prix de base est attribué aux produits mis aux enchères
  - L’utilisateur ne peut pas enchérir négativement
  - L’utilisateur peut suivre un produit sur lequel il a enchérit dans sa page d’achat
  - L’interface Web permet l’achat, la consultation et la mise en vente d’un produit
- Un utilisateur peut être vendeur et/ou acheteur
- Un docker compose a été créé
- La base de données à été créée (avec HSQLdb)

## Installation

### Docker

- Télécharger et extraire le contenu de l'archive.
- Build le fichier .jar de l'application :

```shell
./mvnw install -DskipTests
```

- Construisez ensuite l'image Docker contenant l'application

```shell
docker build .
```

- Lancer ensuite le docker compose

```shell
docker-compose build
docker-compose up
```

### Sources

- Lancer la base de données HSQLdb sur le port par défaut
- Lancer le main contenu dans **src\main\java\fr\progcomp\hepaylike**

## Test

Des données de tests sont insérés dans la BDD au démarrage avec 3 comptes différents :

acheteur@gmail.com - azerty

vendeur@gmail.com - azerty

vendeur2@gmail.com - azerty
